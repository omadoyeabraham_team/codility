/**
 * Determine the factorial of a number.
 *
 * @param {*} number
 */
const factorial = number => {
  let _factorial = 1;
  for (let i = 1; i <= number; i++) {
    _factorial *= i;
  }

  return _factorial;
};

/**
 * Print a triangle of asterisks on the screen
 */
const printAsterix = number => {
  for (let i = 1; i <= number; i++) {
    console.log("* ".repeat(i));
  }

  return;
};

/**
 * Print {number} rows of space seperated asterisks with the following constraints
 *      + Consecutive rows should contain 2n − 1, 2n − 3, . . . , 3, 1 asterisks
 *      + Consecutive rows should be indented by 0, 2, 4, . . . , 2(n − 1) spaces.
 *
 * @param {*} number
 */
const printInvertedAsterix = number => {
  const maximumNumberOfAsterisks = 2 * number - 1;
  for (let i = number; i >= 1; i--) {
    const numberOfAsterisks = 2 * i - 1;
    const numberOfSpaces = maximumNumberOfAsterisks - numberOfAsterisks;
    let result = " ".repeat(numberOfSpaces);
    result += "* ".repeat(numberOfAsterisks);
    console.log(result);
  }
};

/**
 * Get the first {n} fibonnaci numbers
 * @param {*} n
 */
const fibonnaciNumbers = n => {
  let a = 0;
  let b = 1;
  let c;

  while (a <= n) {
    console.log(a);
    c = a + b;
    a = b;
    b = c;
  }
};

(function() {
  //   console.log(factorial(3));
  //   printAsterix(7);
  //   printInvertedAsterix(4);
  console.log(fibonnaciNumbers(50));
})();
