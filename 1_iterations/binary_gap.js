/**
 * Return the binary equivalent of a decimal number
 *
 * @param {*} number
 * @return String
 */
const decimalToBinary = number => {
  return number.toString(2);
};

/**
 * Return the highest number in an array
 * @param {*} numbers
 */
const highestNumberInArray = numbers => {
  numbers = numbers.sort((a, b) => b - a);
  return numbers[0];
};

/**
 * Determine the longest binary gap for a number
 *
 * @link  https://app.codility.com/programmers/lessons/1-iterations/binary_gap/
 * @param {*} N
 */
const binaryGap = N => {
  const binaryString = decimalToBinary(N);
  const length = binaryString.length;
  const binaryGaps = [];

  for (let i = 0; i < length; i++) {
    if (binaryString[i] === "1" && binaryString[i + 1] === "0") {
      //   console.log(i);
      let j = i + 1;
      let numberOfZeroes = 0;
      while (binaryString[j] === "0") {
        j++;
        numberOfZeroes++;
      }

      if (numberOfZeroes > 0 && binaryString[j] === "1") {
        binaryGaps.push(numberOfZeroes);
      }
    }
  }

  if (!binaryGaps.length) {
    return 0;
  } else {
    return highestNumberInArray(binaryGaps);
  }
};

console.log(binaryGap(20));
