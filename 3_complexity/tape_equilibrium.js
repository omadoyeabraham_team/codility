function solution(A) {
  if (!A) return 0;

  const length = A.length;
  let progressiveSum = [];
  let diff;

  for (let i = 0; i < length; i++) {
    if (i === 0) {
      progressiveSum[i] = A[i];
    } else {
      progressiveSum[i] = A[i] + progressiveSum[i - 1];
    }
  }

  for (let i = 0; i < length; i++) {
    if (i !== 0) {
      let _diff =
        progressiveSum[i - 1] -
        (progressiveSum[length - 1] - progressiveSum[i - 1]);
      _diff = Math.abs(_diff);

      if (!diff) {
        diff = _diff;
      } else {
        if (_diff < diff) {
          diff = _diff;
        }
      }
    }
  }

  return diff;
}

console.log(solution([3, 1, 2, 4, 3]));
