/**
 * Determine the minimum number of hops of size {D} that a frog at position {X}
 * needs to take in order to get to position {Y}
 *
 * @param {*} X
 * @param {*} Y
 * @param {*} D
 * @return number
 */
function minFrogHops(X, Y, D) {
  const totalDistance = Y - X;

  if (totalDistance === 0) {
    return 0;
  }

  const numberOfHops = totalDistance / D;

  if (totalDistance % D === 0) {
    return numberOfHops;
  } else {
    return Math.floor(numberOfHops) + 1;
  }
}

console.log(minFrogHops(10, 85, 30));
