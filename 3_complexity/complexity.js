/**
 * Sum 1 + 2 + 3 + .....+ n using recursion
 * @param {*} n
 */
function recursiveSum(n) {
  if (n === 0) {
    return 0;
  }

  if (n === 1) {
    return 1;
  }

  return n + recursiveSum(n - 1);
}

function iterativeSum(n) {
  if (n === 0) {
    return 0;
  }

  if (n === 1) {
    return 1;
  }
  let sum = 0;
  for (let i = 1; i <= n; i++) {
    sum += i;
  }

  return sum;
}

function algebraicSum(n) {
  if (n === 0) {
    return 0;
  }

  if (n === 1) {
    return 1;
  }

  return n * (n + 1) / 2;
}

console.log(recursiveSum(6));
console.log(iterativeSum(6));
console.log(algebraicSum(6));
