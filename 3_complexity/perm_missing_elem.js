/**
 * Find the missing element in an array
 *
 * @param {*} A
 * @return number
 */
function permMissingElem(A) {
  if (!A) {
    return 0;
  }
  const length = A.length + 1;

  if (length === 0) {
    return 1;
  }

  const sumOfAllNumbers = length * (length + 1) / 2;

  const sumOfA = A.reduce((total, currValue) => {
    return total + currValue;
  }, 0);

  return sumOfAllNumbers - sumOfA;
}

console.log(permMissingElem([2, 1, 3, 5]));
