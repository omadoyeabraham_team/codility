/**
 * Rotate an array to the right once
 * @param {*} A
 */
function rotateArray(A) {
  let result = [];
  if (!A.length) {
    return [];
  }

  if (A.length === 1) {
    return A;
  }

  result[0] = A[A.length - 1];

  for (let i = 1; i < A.length; i++) {
    result[i] = A[i - 1];
  }

  return result;
}

/**
 * Rotate an array {A}, {K} times to the right
 *
 * @link
 * @param {Array<number>} A
 * @param {Array<number>} K
 * @return {Array<number>}
 */
function cyclicRotation(A, K) {
  for (let i = 0; i < K; i++) {
    A = rotateArray(A);
  }

  return A;
}

console.log(cyclicRotation([1, 2, 3, 4], 4));
