/**
 * Reverse the contents of an array
 *
 * @param {*} array
 * @return {*} array
 */
function reverseArray(array) {
  const length = array.length;
  for (let i = 0; i < length / 2; i++) {
    const k = length - i - 1;
    const temp = array[i];
    array[i] = array[k];
    array[k] = temp;
  }

  return array;
}

console.log(reverseArray([1, 2, 3, 4, 5, 6, 7, 8, 9]));
