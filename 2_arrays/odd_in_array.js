/**
 * Find the odd occurences in an array
 * @param {*} A
 */
function oddInArray(A) {
  let result = {};
  for (let i = 0; i < A.length; i++) {
    let value = A[i];
    if (result[value]) {
      delete result[value];
    } else {
      result[value] = A[i];
    }
  }

  const oddEntry = result[Object.keys(result)[0]];
  return oddEntry;
}

console.log(oddInArray([3, 3, 4, 5, 5, 4, 6]));
