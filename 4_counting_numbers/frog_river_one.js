function countOccurences(A) {
  let occurences = {};

  for (let i = 0; i < A.length; i++) {
    if (!occurences[A[i]]) {
      occurences[A[i]] = {
        count: 1,
        occurs: [i],
        firstOccurence: i,
        lastOccurence: i
      };
    } else {
      occurences[A[i]].count++;
      occurences[A[i]].occurs.push(i);
      if (i < occurences[A[i]].firstOccurence) {
        occurences[A[i]].firstOccurence = i;
      }
      if (i > occurences[A[i]].lastOccurence) {
        occurences[A[i]].lastOccurence = i;
      }
    }
  }

  return occurences;
}

/**
 *
 * @param {number} X
 * @param {Array<numbers>} A
 */
function frogRiverOne(X, A) {
  var i,
    count = {},
    countIndex = 0,
    index = 0;
  for (i = 0; i < A.length; i++) {
    index = A[i] - 1;
    if (A[i] <= X && !count[index]) {
      count[index] = true;
      countIndex++;
      if (countIndex === X) return i;
    }
  }
  return -1;
}

// console.log(frogRiverOne(5, [1, 3, 1, 4, 2, 3, 5, 4]));
console.log(countOccurences([1, 3, 1, 4, 2, 3, 5, 4]));
