/**
 * Solution for codility's permutation exercise under the couting numbers section
 *
 * @param {Array<number>} A
 * @returns number
 */
function solution(A) {
  let store = [];
  let counter = 0;
  let arrLength = A.length;

  for (let i = 0; i < arrLength; i++) {
    let itemIndexInOrderedArray = A[i] - 1;
    if (!store[itemIndexInOrderedArray] && A[i] <= arrLength) {
      store[itemIndexInOrderedArray] = true;
      counter++;

      if (counter === arrLength) {
        return 1;
      }
    }
  }

  return 0;
}
