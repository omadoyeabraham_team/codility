function shouldIncrease(N, X) {
  return 1 <= X && X <= N;
}

function shouldMaxCounter(N, X) {
  return X === N + 1;
}

function increaseCounter(counterIndex, counters) {
  counters[counterIndex - 1] = counters[counterIndex - 1] + 1;
  return counters;
}

function maxCounters(maximumCounterValue, counters) {
  counters.fill(maximumCounterValue, 0);

  return counters;
}

/**
 * Solve the codility counter problem, under the counting numbers section
 *
 * @param {number} N
 * @param {Array<number>} A
 * @return Array<number>
 */
function solution(N, A) {
  let maximumCounterValue = 0;
  let counters = new Array(N);
  for (let i = 0; i < N; i++) {
    counters[i] = 0;
  }

  for (let i = 0; i < A.length; i++) {
    if (A[i] < N + 1) {
      let newValue = counters[A[i] - 1] + 1;
      counters[A[i] - 1] = newValue;
      if (newValue > maximumCounterValue) {
        maximumCounterValue = newValue;
      }
    } else {
      for (let j = 0; j < counters.length; j++) {
        if (counters[j] !== maximumCounterValue) {
          counters[j] = maximumCounterValue;
        }
      }
    }
  }

  return counters;
}

console.log(solution(5, [1, 2, 3, 4, 6]));
