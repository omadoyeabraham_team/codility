function sum(arr) {
  const arrSum = arr.reduce((total, currValue) => {
    return total + currValue;
  }, 0);

  return arrSum;
}

/**
 * My solution to the Exercise 4.1 in codility's counting numbers PDF
 *
 * @param {Array<number>} A
 * @param {Array<number>} B
 * @param {number} m
 * @returns boolean
 */
function swapNumbers(A, B, m) {
  let sumA = sum(A);
  let sumB = sum(B);
  let difference = Math.abs(sumA - sumB);

  for (let i = 0; i < A.length; i++) {
    for (let j = 0; j < B.length; j++) {
      let diff = Math.abs(A[i] - B[j]);
      if (diff === difference) {
        return true;
      }
    }
  }

  return false;
}
