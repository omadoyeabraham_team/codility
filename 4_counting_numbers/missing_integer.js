function countOccurences(A) {
  let occurences = {};

  for (let i = 0; i < A.length; i++) {
    if (!occurences[A[i]]) {
      occurences[A[i]] = {
        count: 1
      };
    } else {
      occurences[A[i]].count++;
    }
  }

  return occurences;
}

function solution(A) {
  if (!Array.isArray(A)) {
    return 1;
  }

  let index;
  const length = A.length;
  let count = [];
  let occurences = countOccurences(A);

  for (let i = 1; i <= length; i++) {
    if (!occurences[i]) {
      return i;
    }
  }

  return length + 1;
}
